// problem2_var15.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include "stdio.h"
#include <iostream>
#include <iomanip>
#include <stdlib.h> 

using namespace std;

struct Shape{
	int count;
	int* vertexes;
};

Shape* newShape(int v_count, int* vertexes) {
	__asm{
		mov  	eax, 8
		push 	eax
		call 	malloc
		add 	esp, 4 //чистим стек
		
		
		mov		ecx, v_count
		mov 	[eax], ecx
		mov		ecx, vertexes
		mov 	[eax+4], ecx		
	}
}

void delShape(Shape* &shapePtr){
	__asm{
		mov 	eax, shapePtr
		mov 	edi, [eax]
		mov 	ebx,[edi+4]
		push 	edi
		push 	ebx
		call 	free
		pop 	edi
		call 	free
		pop 	edi

		mov		edi, shapePtr
		mov		dword ptr [edi], 0
	}
}



bool shapeComp(Shape* shape1, Shape* shape2) {
	//true если shape1 > shape2
	//null считается наименьшим, два null вернут false

	__asm {
		//смотрим ссылки на фигуры
		//если ссылка первой фигуры null то она точно меньше второй
		cmp		shape1, 0
		je		FALSE
		//если тут значит первая не NULL и если вторая NULL - тогда первая хоть как больше
		cmp		shape2, 0
		je		TRUE
		
		//размещаем адреса первой и второй фигур в esi и edi
		mov 	esi, shape1
		mov 	edi, shape2
		
		//сохраняем число вершин фигур, первой в eax, второй в ebx
		mov 	eax, [esi]
		mov 	ebx, [edi]
		
		//если у первой больше вершин она точно больше
		cmp 	eax, ebx
		jg		TRUE
		//если у второй больше вершин, первая точно меньше
		cmp 	eax, ebx
		jl 		FALSE
		//если попали сюда, значит число вершин одно и то же
		
		//вычисляем в eax индекс крайнего элемента массива вершин
		//те умножаем число вершин на двойку в первой степени
		shl 	eax, 1
		//отнимаем от результата один
		dec 	eax
		//теперь в eax искомый индекс
		
		//обнуляем ecx, это индекс проверяемого элемента
		mov 	ecx, 0
		
		//перекладываем в регистры esi И edi адреса массивов вершин первой и второй фигур
		mov 	ebx, [esi+4]
		mov 	esi, ebx
		mov 	ebx, [edi+4]
		mov 	edi, ebx
		
		//цикл на лексикографич. сравнение массивов вершин
		CYCLE:
			mov 	ebx, [esi][ecx*4]
			mov 	edx, [edi][ecx*4]
			cmp 	ebx, edx
			jg 		TRUE
			
			inc 	ecx
			cmp 	ecx, eax
			
			jg 		FALSE
			
		jmp CYCLE
		
		FALSE:
		mov 	eax, 0
		jmp 	FIN
		
		TRUE:
		mov 	eax, 1
		
		FIN:
		
	}
}

void printShape(Shape* shapePtr) {
	char format_1[] = "Число вершин: %d \n";
	char format_2[] = "[%d, %d] ";
	char format_3[] = "Фигура еще не создана! \n";
	__asm {
		mov		edi, shapePtr
		cmp		edi, 0
		je		NULL_P

		mov		eax, [edi]
		push	eax
		lea		eax, format_1
		push	eax
		call	printf
		add		esp, 4
		
		pop 	ecx
		dec 	ecx
		
		mov		edi, shapePtr
		lea 	edx, format_2
		mov		esi, [edi+4]
		mov		edi, esi
		
		xor		esi, esi
		
		CYCLE:
			cmp 	esi, ecx
			jg 		FIN
			
			push 	esi
			push 	ecx
			push 	edi
			
			mov 	eax,[edi][esi*8+4]
			push 	eax
			mov 	eax,[edi][esi*8]
			push 	eax
			push 	edx
			
			call 	printf
			
			pop 	edx
			add 	esp, 8
			
			pop 	edi
			pop 	ecx
			pop 	esi
			
			inc 	esi
		jmp 	CYCLE


		NULL_P:
		lea		eax, format_3
		push	eax
		call	printf
		add		esp, 4

		FIN:
	}

}


void arraySort(Shape** arr, int size_a){
	__asm {
		mov 	ecx, size_a
		dec 	ecx
		mov 	edx, ecx
		
		mov 	edi, arr

		
		CYCLE:
		
		dec 	ecx
		sub 	edx, ecx
		
		mov 	esi, [edi][edx*4]
		
		push 	ecx
		push 	edx
		
		mov 	ecx, edx
		
			CYCLE_TWO:
				push 	edi
				mov 	edx, [edi][ecx*4-4]
				push 	ecx
				push 	edx
				push 	esi
				
				call  	shapeComp
				
				pop 	esi
				pop 	edx
				pop 	ecx
				pop 	edi
				
				cmp 	eax, 0
				je	 	RIGHT_ORDER
				
				mov 	[edi][ecx*4], edx
				mov		[edi][ecx * 4 - 4], esi
			    
			loop CYCLE_TWO
		
		RIGHT_ORDER:
		

		pop		edx
		pop		ecx
		
		add 	edx, ecx
		inc 	ecx
			
		
		loop CYCLE
		
		
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");

	Shape **mainArr;
	int *temp_int_arr, temp_size, size, temp_elem, menu = 0;

	cout << "Введите размер массива фигур." << endl;
	cin >> size;
	mainArr = new Shape*[size];
	for (int i = 0; i < size;i++) {
		mainArr[i] = nullptr;
	}

	while (menu != 7) {
		cout << "Массив создан. Число фигур - " << size <<endl;
		cout << "1. Создать фигуру." << endl;
		cout << "2. Удалить фигуру." << endl;
		cout << "3. Печать фигуры." << endl;
		cout << "4. Печать массива." << endl;
		cout << "5. Сравнить фигуры. (s1 > s2 ?)" << endl;
		cout << "6. Сортировать массив." << endl;
		cout << "7. Выход." << endl;

		cin >> menu;


		switch (menu) {
		case 1:
			cout << "введите индекс фигуры в массиве (0-" << size-1 << "):" << endl;
			cin >> temp_elem;
			cout << "введите число вершин:" << endl;
			cin >> temp_size;
			cout << "поочередно вводите координаты вершин:" << endl;
			temp_int_arr = new int[temp_size*2];
			for (int n = 0; n < temp_size * 2;n++) {
				cin >> temp_int_arr[n];
			}
			mainArr[temp_elem] = newShape(temp_size, temp_int_arr);
			break;

		case 2:
			cout << "введите индекс фигуры в массиве (0-" << size - 1 << "):" << endl;
			cin >> temp_elem;
			delShape(mainArr[temp_elem]);
			break;

		case 3:
			cout << "введите индекс фигуры в массиве (0-" << size - 1 << "):" << endl;
			cin >> temp_elem;
			cout << temp_elem << ". ";
			printShape(mainArr[temp_elem]);
			system("pause");
			break;
		case 4:
			for (int m = 0; m < size; m++) {
				cout << m<< ". ";
				printShape(mainArr[m]);
				puts("");
			}
			system("pause");
			break;
		case 5:
			cout << "введите индекс фигуры в массиве (0-" << size - 1 << "):" << endl;
			cin >> temp_elem;
			cout << "введите индекс второй фигуры в массиве (0-" << size - 1 << "):" << endl;
			cin >> temp_size;
			cout << shapeComp(mainArr[temp_elem], mainArr[temp_size]);
			break;
		case 6:
			arraySort(mainArr, size);
		}
		system("cls");
	}

	return 0;
}

