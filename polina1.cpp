#include "stdafx.h"
#include "stdio.h"
#include "stdlib.h"
#include "conio.h"

char* problem(int razmerStroki){
	
	int otvet = 0;
	char item =' ';
	__asm {
		
//выделяем память под исходную строку
		mov eax, razmerStroki // размер строки в регистр
		push eax    // отправляем его в стек
		call malloc //вызываем функ
		//сейчас в регистре eax адрес памяти, что выделила malloc
		
//вводим исходную строку
		push eax
		call gets_s
		pop eax
		pop edx

//-------------------------------
		mov ebx,0 //регистр ebx используем как счечтик смещений
		//для передвижения по строке
		//сейчас в регистре eax остается адрес строки
		mov edx, -1
		//  в edx храним позицию последнего отличного от пробела и конца строки 
		// символа, или -1 если стока птрока или из одних пробелов
		
		// cl используем длЯ хранениЯ пробела
		mov cl, item
		
		//byte ptr - указание что тащим из памяти ровно один байт
		MAIN_CYCLE:
			//ищем конец строки
			cmp byte ptr [eax+ebx],0
			je END_C
			//ищем символ пробел
			cmp byte ptr [eax+ebx], cl
			//если символ НЕ пробел то запоминаем его индекс
			je PROBEL
				mov edx, ebx
			PROBEL: //если пробел то ничего не делаем
			
			inc ebx //переходим на след. символ
		jmp MAIN_CYCLE 
		//если новый символ не NULL, цикл продолжается
		//по завершению цикла по адресу [eax+ebx] находится NULL
		END_C:
		
		cmp edx, -1
		jne VSE_OK
			//сюда попали если строка пустая или из пробелов
			mov  byte ptr [eax], 0
			jmp FIN
	
		 VSE_OK: 
			//сюда если все в порядке
			mov byte ptr [eax+edx+1],0
		 
		 FIN:
		
	}	
}

int main()
{
	printf(problem(256));
	printf("|end|");
	_getch();
	return 0;

}